# Diplomatura en ciencia de datos, aprendizaje automático y sus aplicaciones

## Mentoría de detección de objetos en imágenes

### FaMAF 2021

En el presente repositorio se encuentran las entregas a las diferentes materias de
la [diplomatura en ciencias de datos](https://diplodatos.famaf.unc.edu.ar/) de [FaMAF](https://www.famaf.unc.edu.ar/)
cursado en 2021.

La propuesta de la mentoría es trabajar sobre un dataset de imágenes etiquetadas con personas utilizando barbijos o
protectores buconasales y personas que no los utilizan. El objetivo es utilizar dicho dataset como base para las
prácticas de las materias trabajadas a lo largo de la diplomatura.

Para cada materia, el mentor propondrá ciertos objetivos en un Jupyter Notebook y el equipo de trabajo resolverá y
publicará en el presente repositorio los resultados obtenidos.

### Entregas

#### Presentación de las entregas 1 y 2

![Entregas 1 y 2](doc/vids/Entrega_1_2.mp4)

![Entregas 2](doc/vids/Entrega2.mp4)

#### Análisis y visualización de datos

Los resultados y conclusiones de la entrega pueden
encontrarse [aquí](entregas/1_An%C3%A1lisis_y_visulalizaci%C3%B3n.ipynb)

### Dataset

El dataset puede encontrarse en https://gitlab.com/diegobcuadro/mentoria-deteccion-de-objetos-en-imagenes
> Para correr el notebook, es necesario incluir los archivos de `dataset` dentro de la carpeta `dataset` de este repositorio

Está extraido de diversos videos públicos que contienen personas utilizando en algunos casos protectores buconazales y
en otros casos no.

|![Personas utilizando protector](doc/img/video4-frame-000039.jpg "Persona utilizando protector") | ![Personas sin utilizar protector](doc/img/video5-frame-000281.jpg "Persona sin utilizar protector") |
|---|---|

De los videos se extrajo un dataset de training y un muestreo para utilizar como tests.

Para enriquecer el dataset en vistas de las necesidades de las materias a cursar, se agregaron *features* extraidos
con [pytorch](https://pytorch.org/) y [ResNet-18](https://arxiv.org/abs/1512.03385). Puede encontrarse detalle sobre el
proceso en [este artículo](https://becominghuman.ai/extract-a-feature-vector-for-any-image-with-pytorch-9717561d1d4c).

### Inicialización del entorno

Los análisis y entregas se hacen en [jupyter notebook](https://jupyter.org/). Se puede consultar la documentación para
la instalación de jupyter y dependencias.

En el presente repositorio, se ofrece un archivo [`pyproject.toml`](pyproject.toml) para instalar
con [poetry](https://python-poetry.org/) o algún otro manejador de dependencias.

En caso de instalar con poetry, se debe [instalar poetry](https://python-poetry.org/docs/#installation) en primer lugar
y luego correr los siguientes comandos

```bash
poetry install
poetry run jupyter notebook
```

#### Dependencias

En caso de hacer una instalación manual, el proyecto tiene las siguientes dependencias:

- [Pandas](https://pandas.pydata.org/)
- [Seaborn](https://seaborn.pydata.org/)
- [opencv-python](https://github.com/opencv/opencv-python)
- [sklearn](https://scikit-learn.org/)
- [scikit-image](https://scikit-image.org/)
- [statsmodels](https://www.statsmodels.org/)

### Autores

- Alvarez, Juan Antonio
- Devesa, María Roberta
