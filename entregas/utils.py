import itertools
import numpy as np
import matplotlib.pyplot as plt


def plot_decision_boundary(model, X, y, ax=None):
    X = X.T
    y = y.T

    # Set min and max values and give it some padding
    x_min, x_max = X[0, :].min(), X[0, :].max()
    y_min, y_max = X[1, :].min(), X[1, :].max()
    x_pad = (x_max - x_min) * .1
    y_pad = (y_max - y_min) * .1
    x_min = x_min - x_pad
    x_max = x_max + x_pad
    y_min = y_min - y_pad
    y_max = y_max + y_pad
    h = 0.01

    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Predict the function value for the whole grid
    Z = model(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    # Plot the contour and training examples
    if ax is None:
        plt.figure()
        ax = plt.axes()
    ax.set_title("Fronteras de decisión")
    ax.contourf(xx, yy, Z, cmap=plt.cm.ocean)
    ax.scatter(X[0, y==1], X[1, y==1], color="dodgerblue", edgecolors='k', label="1")
    ax.scatter(X[0, y==-1], X[1, y==-1], color="tomato", edgecolors='k', label="-1")
    ax.legend()


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues, ax=None):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if ax is None:
        plt.figure()
        ax = plt.axes()
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.set_title(title)
    # ax.colorbar()
    tick_marks = np.arange(len(classes))
    ax.set_xticks(tick_marks)
    ax.set_xticklabels(classes)
    ax.set_yticks(tick_marks)
    ax.set_yticklabels(classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        ax.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    ax.set_ylabel('Etiqueta correcta')
    ax.set_xlabel('Etiqueta predicha')
